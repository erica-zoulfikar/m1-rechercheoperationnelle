# routes.py
from fastapi import APIRouter, HTTPException, status
from backend import *  # Import your backend functions

api_router = APIRouter()

@api_router.get("/")
def read_root():
    return {"Hello": "World"}

@api_router.post("/set_choice", response_model=None)
async def set_choice(choice):
    if choice not in ['0', '1']:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid choice")
    return {"message": "Choice set successfully"}