import numpy as np
from scipy.optimize import linprog

def branch_and_bound(c, A, b, bounds, integer_vars):
    """
    Résolution d'un problème de PLNE par la méthode Branch-and-Bound.

    :param c: Coefficients de la fonction objectif (doit être maximisé)
    :param A: Matrice des contraintes
    :param b: Vecteur des termes constants des contraintes
    :param bounds: Bornes des variables
    :param integer_vars: Liste des indices des variables qui doivent être entières
    :return: Solution optimale
    """

    def is_integer_solution(x, integer_vars):
        """ Vérifie si les variables entières sont bien des entiers """
        return all(np.isclose(x[i], int(x[i])) for i in integer_vars)

    def solve_relaxed_problem(current_A, current_b, current_bounds):
        """ Résout le problème de relaxation linéaire """
        result = linprog(c, A_ub=current_A, b_ub=current_b, bounds=current_bounds, method='highs')
        return result

    best_solution = None
    best_value = -np.inf

    stack = [(A, b, bounds)]

    while stack:
        current_A, current_b, current_bounds = stack.pop()
        result = solve_relaxed_problem(current_A, current_b, current_bounds)
        
        if not result.success:
            continue
        
        x = result.x
        print("\nSolution relaxée actuelle:", x)
        print("Valeur de la fonction objectif:", -result.fun)
        
        if result.fun > best_value and is_integer_solution(x, integer_vars):
            best_solution = x
            best_value = result.fun

        # Branching
        branched = False
        for var_index in integer_vars:
            if not np.isclose(x[var_index], int(x[var_index])):
                lower_bound = int(np.floor(x[var_index]))
                upper_bound = int(np.ceil(x[var_index]))
                
                new_bounds1 = current_bounds.copy()
                new_bounds1[var_index] = (current_bounds[var_index][0], lower_bound)
                
                new_bounds2 = current_bounds.copy()
                new_bounds2[var_index] = (upper_bound, current_bounds[var_index][1])
                
                stack.append((current_A, current_b, new_bounds1))
                stack.append((current_A, current_b, new_bounds2))
                branched = True
                break
        
        if not branched:
            print("Aucune branche valide trouvée. Terminé.")
            break

    return best_solution, -best_value

# Exemple du document
c = [-10, -14, -12]
A = [
    [1, 3, 2],
    [3, 2, 1],
    [1, 1, 4]
]
b = [40, 45, 38]
bounds = [(0, None), (0, None), (0, None)]
integer_vars = [0, 1, 2]

solution, value = branch_and_bound(c, A, b, bounds, integer_vars)
print("\nSolution optimale:", solution)
print("Valeur optimale:", value)
