import numpy as np
from scipy.optimize import linprog

def branch_and_bound_manual(c, A, b, bounds, integer_vars):
    """
    Résolution d'un problème de PLNE par la méthode Branch-and-Bound avec interaction manuelle.

    :param c: Coefficients de la fonction objectif (doit être maximisé)
    :param A: Matrice des contraintes
    :param b: Vecteur des termes constants des contraintes
    :param bounds: Bornes des variables
    :param integer_vars: Liste des indices des variables qui doivent être entières
    :return: Solution optimale
    """

    def is_integer_solution(x, integer_vars):
        """ Vérifie si les variables entières sont bien des entiers """
        return all(x[i] == int(x[i]) for i in integer_vars)

    def solve_relaxed_problem(A, b, bounds):
        """ Résout le problème de relaxation linéaire """
        result = linprog(c, A_ub=A, b_ub=b, bounds=bounds, method='highs')
        return result

    best_solution = None
    best_value = -np.inf

    stack = [(A, b, bounds)]

    while stack:
        current_A, current_b, current_bounds = stack.pop()
        result = solve_relaxed_problem(current_A, current_b, current_bounds)
        
        if not result.success:
            continue
        
        x = result.x
        print("\nSolution relaxée actuelle:", x)
        print("Valeur de la fonction objectif:", -result.fun)
        
        if result.fun > best_value and is_integer_solution(x, integer_vars):
            best_solution = x
            best_value = result.fun

        # Branching
        branched = False
        for var_index in integer_vars:
            if x[var_index] != int(x[var_index]):
                lower_bound = int(np.floor(x[var_index]))
                upper_bound = int(np.ceil(x[var_index]))
                
                print(f"\nBranching sur la variable x{var_index + 1}: {x[var_index]}")
                choice = input(f"Choisissez la branche (0 pour x{var_index + 1} ≤ {lower_bound}, 1 pour x{var_index + 1} ≥ {upper_bound}): ")

                if choice == '0':
                    new_bounds = current_bounds.copy()
                    new_bounds[var_index] = (current_bounds[var_index][0], lower_bound)
                elif choice == '1':
                    new_bounds = current_bounds.copy()
                    new_bounds[var_index] = (upper_bound, current_bounds[var_index][1])
                else:
                    print("Choix invalide. Continuer avec la branche par défaut.")
                    continue

                stack.append((current_A, current_b, new_bounds))
                branched = True
                break

        if not branched:
            print("Aucune branche valide trouvée. Terminé.")

    return best_solution, -best_value

# Exemple du document
c = [-10, -14, -12]
A = [
    [1, 3, 2],
    [3, 2, 1],
    [1, 1, 4]
]
b = [40, 45, 38]
bounds = [(0, None), (0, None), (0, None)]
integer_vars = [0, 1, 2]

solution, value = branch_and_bound_manual(c, A, b, bounds, integer_vars)
print("\nSolution optimale:", solution)
print("Valeur optimale:", value)