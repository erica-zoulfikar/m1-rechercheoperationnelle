def dijkstra(graph, source):
    # Initialisation des distances avec l'infini pour tous les sommets
    distances = {vertex: float('inf') for vertex in graph}
    
    # Distance de la source à elle-même est 0
    distances[source] = 0
    
    # Initialisation des ensembles S (sommets visités) et S_ (sommets non visités)
    S = set()       # Ensemble vide pour les sommets visités
    S_ = set(graph.keys())  # Ensemble de tous les sommets du graphe
    
    while S_:
        # Sélectionner le sommet j dans S_ avec la distance minimale
        min_vertex = None
        for vertex in S_:
            if min_vertex is None or distances[vertex] < distances[min_vertex]:
                min_vertex = vertex
        
        # Si la distance minimale est infinie, on arrête (graphe non connecté)
        if distances[min_vertex] == float('inf'):
            break
        
        # Ajouter le sommet avec la distance minimale à S et le retirer de S_
        S_.remove(min_vertex)
        S.add(min_vertex)
        
        # Mettre à jour les distances des voisins de min_vertex
        for neighbor, weight in graph[min_vertex].items():
            if neighbor in S_:  # Seulement pour les sommets non visités
                new_distance = distances[min_vertex] + weight
                if new_distance < distances[neighbor]:
                    distances[neighbor] = new_distance
    
    return distances

# Exemple d'utilisation
graph = {
    'A': {'C': 3, 'D': 4, 'E': 4},
    'B': {'C': 2, 'F': 2},
    'C': {'A': 3, 'B': 2, 'E': 4, 'F': 5, 'G': 5},
    'D': {'A': 4, 'E': 2},
    'E': {'A': 4, 'C': 4, 'G': 5},
    'F': {'B': 2, 'C': 5, 'G': 5},
    'G': {'C': 5, 'E': 5, 'F': 5},
}

source = 'D'
distances = dijkstra(graph, source)
print(distances)
