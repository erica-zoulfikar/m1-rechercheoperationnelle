def print_matrix(matrix):
    for row in matrix:
        print('\t'.join(map(str, [f'{e:.2f}' for e in row])))
    print()

def is_optimal(list):
    if any(element < 0 for element in list[:-1]):
        return False
    return True
    
def find_pivot_col(list):
    return list.index(min(list))
    
def find_pivot_line(matrix, col):
    line = 0
    min_ratio = matrix[0][-1] / matrix[0][col]
    for i in range(len(matrix[:-1])):
            ratio = matrix[i][-1] / matrix[i][col]
            if ratio < min_ratio:
                line = i
    return line

def find_variable_sortante(matrix):
    col = find_pivot_col(matrix[-1])
    line = find_pivot_line(matrix, col)
    return line, col

def pivot(matrix):
    # Divisez la ligne du pivot par son coef pour rendre le pivot égal à 1
    line, col = find_variable_sortante(matrix)
    pivot_row = [element / matrix[line][col] for element in matrix[line]]
    for i in range(len(matrix)):
        if matrix[i] != matrix[line]:
            coef = matrix[i][col]
            if coef < 0:
                matrix[i] = [matrix[i][j] + ((coef * -1) * pivot_row[j]) for j in range(len(matrix[i]))]
            else:
                matrix[i] = [matrix[i][j] - (coef * pivot_row[j]) for j in range(len(matrix[i]))]
                            
    matrix[line] = pivot_row


# Max 3x1 + 2x2  Objectif
# 2x1 + x2 ≤ 100
# x1 + x2 ≤ 80
# x1, x2 ≥ 0  Contraintes
def ex1(simplex_matrix):
    print_matrix(simplex_matrix)
    pivot(simplex_matrix)
    print_matrix(simplex_matrix)
    while not is_optimal(simplex_matrix[-1]):
        pivot(simplex_matrix)
        print_matrix(simplex_matrix)
    
    last_col = [row[-1] for row in simplex_matrix]
    result = [last_col[i] if simplex_matrix[-1][i] == 0 else 0 for i, x in enumerate(last_col[:-1])]
    result.append(last_col[-1])
    return result

# simplex_matrix = [
#     [2, 1, 1, 0, 100],
#     [1, 1, 0, 1, 80],
#     [-3, -2, 0, 0, 0]
# ]

simplex_matrix = [
    [1, 3, 2, 1, 0, 0, 40],
    [3, 2, 1, 0, 1, 0, 45],
    [1, 1, 4, 0, 0, 1, 38],
    [-10, -14, -12, 0, 0, 0, 0],
]

print([f'{e:.2f}' for e in ex1(simplex_matrix)])