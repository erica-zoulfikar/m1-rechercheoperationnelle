def coloration_graphe(graphe):
    # Calcul des degrés des sommets
    degres = {sommet: len(adj) for sommet, adj in graphe.items()}
    
    # Tri des sommets par ordre de degré décroissant
    sommets_tries = sorted(degres, key=degres.get, reverse=True)
    
    # Initialisation des structures de données
    coloration = {}  # Dictionnaire pour stocker la couleur de chaque sommet
    couleurs_utilisees = []  # Liste pour suivre les couleurs utilisées
    
    # Coloration des sommets
    for sommet in sommets_tries:
        couleurs_interdites = {coloration[adj] for adj in graphe[sommet] if adj in coloration}
        
        for couleur in couleurs_utilisees:
            if couleur not in couleurs_interdites:
                coloration[sommet] = couleur
                break
        else:
            nouvelle_couleur = len(couleurs_utilisees) + 1
            coloration[sommet] = nouvelle_couleur
            couleurs_utilisees.append(nouvelle_couleur)
    
    # Détermination des ensembles stables
    stables = []
    for couleur in range(1, max(coloration.values()) + 1):
        stable = [sommet for sommet, col in coloration.items() if col == couleur]
        stables.append(stable)
    
    # Attribution des couleurs aux ensembles stables
    couleur_stables = {}
    couleurs_disponibles = list(range(1, max(coloration.values()) + 2))  # Couleurs disponibles pour les stables
    
    for i, stable in enumerate(stables, start=1):
        couleur_stable = couleurs_disponibles[i - 1]
        for sommet in stable:
            couleur_stables[sommet] = couleur_stable
    
    # Détermination du nombre chromatique
    nombre_chromatique = len(stables)
    
    return coloration, stables, couleur_stables, nombre_chromatique

# Exemple d'utilisation
graphe = {
    # 'A': ['B', 'E', 'C'],
    # 'B': ['A', 'E', 'D'],
    # 'C': ['A', 'E'],
    # 'D': ['B'],
    # 'E': ['A', 'B', 'C']

    'A': ['B', 'D', 'C', 'G'],
    'B': ['A','C', 'D', 'E','G'],
    'C': ['A','D', 'F', 'G', 'B'],
    'D': ['A','E', 'F', 'B', 'C'],
    'E': ['F', 'G', 'B', 'D'],
    'F': ['G', 'C', 'D', 'E'],
    'G':['A', 'B', 'C', 'E','F']
}

coloration, stables, couleur_stables, nombre_chromatique = coloration_graphe(graphe)

# Affichage des résultats
print("Coloration des sommets :")
print(coloration)

print("\nEnsembles stables (groupes de sommets non adjacents) :")
for stable in stables:
    print(stable)

print("\nNombre chromatique du graphe :", nombre_chromatique)
