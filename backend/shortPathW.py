class Graph:
    def __init__(self, size):
        # Initialise un graphe avec une matrice d'adjacence de taille 'size'
        self.adj_matrix = [[0] * size for _ in range(size)]
        self.size = size
        self.vertex_data = [''] * size  # Liste pour stocker les données des sommets

    def add_edge(self, u, v, weight):
        # Ajoute une arête avec un poids entre les sommets u et v (graphe non orienté)
        if 0 <= u < self.size and 0 <= v < self.size:
            self.adj_matrix[u][v] = weight
            self.adj_matrix[v][u] = weight  # Pour un graphe non orienté

    def add_vertex_data(self, vertex, data):
        # Ajoute des données à un sommet spécifique
        if 0 <= vertex < self.size:
            self.vertex_data[vertex] = data

    def dijkstra(self, start_vertex_data):
        # Trouve le sommet de départ en utilisant les données du sommet
        start_vertex = self.vertex_data.index(start_vertex_data)
        # Initialise les distances avec l'infini
        distances = [float('inf')] * self.size
        # La distance à la source est de 0
        distances[start_vertex] = 0
        # Liste pour suivre les sommets visités
        visited = [False] * self.size

        # Pour chaque sommet dans le graphe
        for _ in range(self.size):
            # Trouver le sommet avec la distance minimale parmi les non visités
            min_distance = float('inf')
            u = None
            for i in range(self.size):
                if not visited[i] and distances[i] < min_distance:
                    min_distance = distances[i]
                    u = i

            # Si aucun sommet n'est accessible, on arrête
            if u is None:
                break

            # Marque le sommet comme visité
            visited[u] = True

            # Met à jour les distances pour les voisins du sommet courant
            for v in range(self.size):
                if self.adj_matrix[u][v] != 0 and not visited[v]:
                    alt = distances[u] + self.adj_matrix[u][v]
                    if alt < distances[v]:
                        distances[v] = alt

        # Retourne les distances les plus courtes depuis le sommet de départ
        return distances

# Création d'un graphe avec 7 sommets
g = Graph(7)

# Ajout de données pour chaque sommet
g.add_vertex_data(0, 'A')
g.add_vertex_data(1, 'B')
g.add_vertex_data(2, 'C')
g.add_vertex_data(3, 'D')
g.add_vertex_data(4, 'E')
g.add_vertex_data(5, 'F')
g.add_vertex_data(6, 'G')

# Ajout des arêtes entre les sommets avec les poids spécifiés
g.add_edge(3, 0, 4)  # D - A, poids 4
g.add_edge(3, 4, 2)  # D - E, poids 2
g.add_edge(0, 2, 3)  # A - C, poids 3
g.add_edge(0, 4, 4)  # A - E, poids 4
g.add_edge(4, 2, 4)  # E - C, poids 4
g.add_edge(4, 6, 5)  # E - G, poids 5
g.add_edge(2, 5, 5)  # C - F, poids 5
g.add_edge(2, 1, 2)  # C - B, poids 2
g.add_edge(1, 5, 2)  # B - F, poids 2
g.add_edge(6, 5, 5)  # G - F, poids 5

# Exécution de l'algorithme de Dijkstra à partir du sommet
print("Algorithme de Dijkstra à partir du sommet :\n")
distances = g.dijkstra('D')
# Affichage des distances les plus courtes de D à chaque autre sommet
for i, d in enumerate(distances):
    print(f"Le plus court chemin de la source à {g.vertex_data[i]}: {d}")
