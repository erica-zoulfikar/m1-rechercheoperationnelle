class Graph:
    def __init__(self, vertices):
        self.V = vertices  # Nombre de nœuds dans le graphe
        self.graph = [[0] * self.V for _ in range(self.V)]  # Matrice d'adjacence du graphe

    def add_edge(self, u, v, capacity):
        self.graph[u][v] = capacity

    # Fonction pour effectuer une recherche BFS pour trouver un chemin augmentant C'est un algorithme de parcours de graphe qui commence au niveau le plus proche du nœud de départ (ou des nœuds) et explore ensuite les voisins à ce niveau avant de passer aux niveaux suivants de manière successive.
    
    def bfs(self, s, t, parent):
        visited = [False] * self.V
        queue = [s]
        visited[s] = True

        while queue:
            u = queue.pop(0)

            for v in range(self.V):
                if visited[v] == False and self.graph[u][v] > 0:
                    queue.append(v)
                    visited[v] = True
                    parent[v] = u
                    if v == t:
                        return True
        return False

    # Algorithme de Ford-Fulkerson pour trouver le flot maximal de s à t
    def ford_fulkerson(self, source, sink):
        parent = [-1] * self.V
        max_flow = 0

        while self.bfs(source, sink, parent):
            path_flow = float('Inf')
            s = sink
            while s != source:
                path_flow = min(path_flow, self.graph[parent[s]][s])
                s = parent[s]

            # Mise à jour des capacités résiduelles des arêtes et des arêtes inverses
            v = sink
            while v != source:
                u = parent[v]
                self.graph[u][v] -= path_flow
                self.graph[v][u] += path_flow
                v = parent[v]

            max_flow += path_flow

            # Réinitialisation du tableau parent pour la prochaine recherche BFS
            parent = [-1] * self.V

        return max_flow


# Exemple d'utilisation
if __name__ == '__main__':
    # Création d'un graphe avec 6 nœuds
    graph = Graph(6)
    
    # Ajout des arêtes avec leurs capacités
    graph.add_edge(0, 1, 16)
    graph.add_edge(0, 2, 13)
    graph.add_edge(1, 2, 10)
    graph.add_edge(1, 3, 12)
    graph.add_edge(2, 1, 4)
    graph.add_edge(2, 4, 14)
    graph.add_edge(3, 2, 9)
    graph.add_edge(3, 5, 20)
    graph.add_edge(4, 3, 7)
    graph.add_edge(4, 5, 4)

    source = 0    # Source
    sink = 5      # Puits

    print("Le flot maximal est :", graph.ford_fulkerson(source, sink))
