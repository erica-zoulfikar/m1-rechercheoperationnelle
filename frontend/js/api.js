function helloWorld() {
    $.ajax({
        url: 'http://127.0.0.1:8000/',
        method: 'GET',
        contentType: 'application/json',
        success: function(response) {
            alert("Hello World!");
            console.log(response);
        },
        error: function(xhr, status, error) {
            console.error('Error:', status, error);
        }
    });
}