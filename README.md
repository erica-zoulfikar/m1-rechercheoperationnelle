## Installation

```
pip install requirements.txt
```

- installes **_live server_** extension sur VSCode

## Lancement

```
uvicorn main:app --reload 
```
- ouvres la page **_index.html_** dans VSCode et tu lance le live server