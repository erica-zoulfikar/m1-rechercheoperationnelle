from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routes import api_router  # Import the router from routes.py

app = FastAPI()

app.include_router(api_router)

# CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Allow all origins
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)